/*******************************************************************************
 * CSE360 Project: To-Do List Unlimited 2019
 * File: ErrorWindows.java
 * @authors: Thomas Shelley, Rebecca Martin, William Bowers, Robert Haas
 * Description: This class creates and displays the pop up windows
 *              containing error messages.
 ******************************************************************************/

package CSE360Proj;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

class ErrorWindows {

	void errorWindow(String error){

		Stage errorStage = new Stage();
		Label errorMessage = new Label();

		errorStage.initModality(Modality.APPLICATION_MODAL);
		errorStage.setTitle("Error");
		errorStage.setMinWidth(250);

		switch (error) {
			case "Duplicate Name":
				errorMessage.setText("Duplicate Name: \n\n Please enter a unique name!");
				break;
			case "Duplicate Description":
				errorMessage.setText("Duplicate Description: \n\n Please enter a unique description!");
				break;
            case "Failed to Print":
                errorMessage.setText("Failed to Print.");
                break;
			case "Invalid Priority":
				errorMessage.setText("Invalid Priority: \n\n You don't have that many tasks!");
				break;
			case "Non-numeric Priority":
				errorMessage.setText("Invalid Priority: \n\n Please enter an integer!");
				break;
			case "Invalid Selection":
				errorMessage.setText("Please select a task!");
				break;
			case "Invalid Date":
				errorMessage.setText("Please enter a date!");
				break;
			case "Missing Info":
				errorMessage.setText("Missing information: \n\n Please enter all fields!");
				break;
            case "Print Canceled":
                errorMessage.setText("Print Canceled.");
				break;
			case "File Not Found":
				errorMessage.setText("Error: File Not Found");
				ToDoListUnlimited.filename = "";
				break;
		}

		BorderPane layout = new BorderPane();
		layout.setMinSize(400,200);
		layout.setCenter(errorMessage);

		Scene scene = new Scene(layout);
		errorStage.setScene(scene);
		errorStage.showAndWait();
	}
}
