/*******************************************************************************
 * CSE360 Project: To-Do List Unlimited 2019
 * File: changeInfo.java
 * @authors: Thomas Shelley, Rebecca Martin, William Bowers, Robert Haas
 * Description: This class handles all of the updating functionality, as well
 *              as the pop ups for saving and opening files.  
 ******************************************************************************/

package CSE360Proj;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;

class changeInfo {
	private static TextField nameTF;
	private static TextArea descTA;
	private static TextField priorityTF;
	private static DatePicker dueDateDP;
	private static ComboBox<String> statusCB;

	static void updateButtonWindow(){
		Label nameLabel;
		Label descLabel;
		Label priorityLabel;
		Label dueDateLabel;
		Label statusLabel;

		Button changeButton;
		Button cancelButton;

		Stage updateWindow = new Stage();

		updateWindow.initModality(Modality.APPLICATION_MODAL);
		updateWindow.setTitle("Make Changes");
		updateWindow.setMinWidth(250);

		nameLabel = new Label(" Name: ");
		descLabel = new Label(" Description: ");
		priorityLabel = new Label(" Priority: ");
		dueDateLabel = new Label(" Due Date: ");
		statusLabel = new Label(" Status: ");

		nameTF = new TextField();
		descTA = new TextArea();
		double heightTA = 60, widthTA = 10;
		descTA.setPrefHeight(heightTA);
		descTA.setPrefWidth(widthTA);

		priorityTF = new TextField();
		dueDateDP = new DatePicker();

		statusCB = new ComboBox<String>();
		statusCB.getItems().addAll(
				"Not Started",
				"In Progress",
				"Finished"
		);

		GridPane inputGrid = new GridPane();
		inputGrid.setMinSize(50, 50);
		inputGrid.setAlignment(Pos.CENTER);
		inputGrid.setHgap(20);
		inputGrid.setVgap(5);
		inputGrid.add(nameLabel, 0, 0);
		inputGrid.add(nameTF, 1, 0);
		inputGrid.add(descLabel, 0, 1);
		inputGrid.add(descTA, 1, 1);
		inputGrid.add(priorityLabel, 0, 2);
		inputGrid.add(priorityTF, 1, 2);
		inputGrid.add(dueDateLabel, 0, 3);
		inputGrid.add(dueDateDP, 1, 3);
		inputGrid.add(statusLabel, 0, 4);
		inputGrid.add(statusCB, 1, 4);

		changeButton = new Button("Change");
		int selected = ToDoListUnlimited.todoList.getSelectionModel().getSelectedIndex();

		changeButton.setOnAction(e -> {
			ErrorWindows error = new ErrorWindows();
			boolean validInput = true;

			if(nameTF.getText().equals("") || descTA.getText().equals("") ||
			   priorityTF.getText().equals("") || statusCB.getValue() == null){
				error.errorWindow("Missing Info");
				validInput = false;
			}

			else if(dueDateDP.getValue() == null){
				error.errorWindow("Invalid Date");
				validInput = false;
			}

			else {
				validInput = ToDoListUnlimited.updateTask(selected);
			}

			if (validInput)
				updateWindow.close();
		});

		cancelButton = new Button("Cancel");
		cancelButton.setOnAction(e -> updateWindow.close());

		HBox buttonBox = new HBox();
		buttonBox.setAlignment(Pos.CENTER);
		buttonBox.setSpacing(5);
		buttonBox.getChildren().add(changeButton);
		buttonBox.getChildren().add(cancelButton);

		VBox layout = new VBox(20);
		layout.setMinSize(300, 300);
		layout.setAlignment(Pos.CENTER);
		layout.getChildren().add(inputGrid);
		layout.getChildren().add(buttonBox);

		Scene scene = new Scene(layout);
		updateWindow.setScene(scene);
		updateWindow.showAndWait();
	}

	static boolean update(int selectIndex, List selectTask, ArrayList<List> taskList) {
		String name = nameTF.getText();
		String desc = descTA.getText();
		String date = dueDateDP.getValue().toString();
		String status = statusCB.getValue();
		boolean duplicateName = false;
		boolean duplicateDescription = false;
		boolean validInput = true;

		ErrorWindows error = new ErrorWindows();
		int priority = selectTask.getPriority();

		try {
			priority = Integer.parseInt(priorityTF.getText());
		} catch (NumberFormatException e) {
			error.errorWindow("Non-numeric Priority");
			validInput = false;
		}

		for (List task: taskList) {
			if (taskList.indexOf(task) != selectIndex) {
				if (task.getName().equals(name)) {
					duplicateName = true;
				}

				if (task.getDesc().equals(desc)) {
					duplicateDescription = true;
				}
			}
		}

		if (priority > selectTask.getPriority()) {
			for (List task: taskList) {
				if (task.getPriority() > selectTask.getPriority() && task.getPriority() <= priority) {
					task.setPriority(task.getPriority() - 1);
				}
			}
		}

		else if (priority < selectTask.getPriority()) {
			for (List task: taskList) {
				if (task.getPriority() < selectTask.getPriority() && task.getPriority() >= priority) {
					task.setPriority(task.getPriority() + 1);
				}
			}
		}

		if (duplicateName) {
			error.errorWindow("Duplicate Name");
			validInput = false;
		}

		else if (duplicateDescription) {
			error.errorWindow("Duplicate Description");
			validInput = false;
		}

		else if (priority > taskList.size()) {
			error.errorWindow("Invalid Priority");
			validInput = false;
		}

		else {
			selectTask.setName(name);
			selectTask.setDesc(desc);
			selectTask.setPriority(priority);
			selectTask.setDueDate(date);
			selectTask.setStatus(status);
		}

		return validInput;
	}

	static void savedPrintedWindow(String action){
		Label label = new Label();
		Button okButton;

		Stage window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		window.setMinWidth(250);

		switch(action) {
			case "Save":
				window.setTitle("Saved");
				label.setText("To-Do list saved to '" + ToDoListUnlimited.filename + "'.");
				break;
			case "Print":
				window.setTitle("Printed");
				label.setText("To-Do list printed to '" + ToDoListUnlimited.filename + "'.");
				break;
		}

		okButton = new Button("OK");
		okButton.setOnAction(e -> window.close());

		VBox layout = new VBox(20);
		layout.setMinSize(100, 100);
		layout.setAlignment(Pos.CENTER);
		layout.getChildren().add(label);
		layout.getChildren().add(okButton);

		Scene scene = new Scene(layout);
		window.setScene(scene);
		window.showAndWait();
	}

	static void chooseFromTwoWindow(String options){
		Label label = new Label();
		Button leftButton = new Button();
		Button rightButton = new Button();

		Stage window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		window.setMinWidth(250);

		switch (options) {
			case "Save/Discard":
				window.setTitle("Save or Discard");
				label.setText("Do you want to save your changes?");

				leftButton.setText("Save");
				leftButton.setOnAction(e -> {
					ToDoListUnlimited.saveList();
					window.close();
				});

				rightButton.setText("Discard");
				rightButton.setOnAction(e -> window.close());
				break;
			case "StartOver/Cancel":
				window.setTitle("Start Over?");
				label.setText("  Are you sure you want to start over? Any unsaved changes will be lost.  ");

				leftButton = new Button("Start Over");
				leftButton.setOnAction(e -> {
					ToDoListUnlimited.startOver();
					window.close();
				});

				rightButton = new Button("Cancel");
				rightButton.setOnAction(e -> window.close());
				break;
		}

		HBox buttonBox = new HBox();
		buttonBox.setAlignment(Pos.CENTER);
		buttonBox.setSpacing(5);
		buttonBox.getChildren().add(leftButton);
		buttonBox.getChildren().add(rightButton);

		VBox saveLayout = new VBox(20);
		saveLayout.setMinSize(100, 100);
		saveLayout.setAlignment(Pos.CENTER);
		saveLayout.getChildren().add(label);
		saveLayout.getChildren().add(buttonBox);

		Scene scene = new Scene(saveLayout);
		window.setScene(scene);
		window.showAndWait();
	}

	static void openFile() {
		Label label;
		Button openButton;
		Button createNewButton;
		TextField fileName = new TextField();
		fileName.setMinSize(200, 25);
		fileName.setMaxSize(200, 25);

		Stage openFileWindow = new Stage();

		openButton = new Button("Open File");
		openButton.setOnAction(e -> {
			String newFileName = fileName.getText() + ".txt";
			ToDoListUnlimited.filename = newFileName;
			if (ToDoListUnlimited.restoreList(newFileName))
				openFileWindow.close();
		});

		createNewButton = new Button("Create New List");
		createNewButton.setOnAction(e -> openFileWindow.close());

		openFileWindow.initModality(Modality.APPLICATION_MODAL);
		openFileWindow.setTitle("Open a File");
		openFileWindow.setMinWidth(250);

		label = new Label("Enter the name of the file you want to open:");

		HBox buttonBox = new HBox();
		buttonBox.setAlignment(Pos.CENTER);
		buttonBox.setSpacing(5);
		buttonBox.getChildren().add(openButton);
		buttonBox.getChildren().add(createNewButton);

		VBox layout = new VBox(20);
		layout.setMinSize(300, 300);
		layout.setAlignment(Pos.CENTER);
		layout.getChildren().add(label);
		layout.getChildren().add(fileName);
		layout.getChildren().add(buttonBox);

		Scene scene = new Scene(layout);
		openFileWindow.setScene(scene);
		openFileWindow.showAndWait();
	}

	static void nameFile() {
		Label label;
		Button enterButton;
		TextField fileName = new TextField("UntitledList");
		fileName.setMinSize(200, 25);
		fileName.setMaxSize(200, 25);

		Stage openFileWindow = new Stage();

		enterButton = new Button("Enter");
		enterButton.setOnAction(e -> {
			ToDoListUnlimited.filename = fileName.getText() + ".txt";
			openFileWindow.close();
		});

		openFileWindow.initModality(Modality.APPLICATION_MODAL);
		openFileWindow.setTitle("Name File");
		openFileWindow.setMinWidth(250);

		label = new Label("Enter a name for your list:");

		HBox buttonBox = new HBox();
		buttonBox.setAlignment(Pos.CENTER);
		buttonBox.setSpacing(5);
		buttonBox.getChildren().add(enterButton);

		VBox layout = new VBox(20);
		layout.setMinSize(300, 300);
		layout.setAlignment(Pos.CENTER);
		layout.getChildren().add(label);
		layout.getChildren().add(fileName);
		layout.getChildren().add(buttonBox);

		Scene scene = new Scene(layout);
		openFileWindow.setScene(scene);
		openFileWindow.showAndWait();
	}
}
