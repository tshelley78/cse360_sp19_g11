/*******************************************************************************
 * CSE360 Project: To-Do List Unlimited 2019
 * File: ToDoListUnlimited.java
 * @authors: Thomas Shelley, Rebecca Martin, William Bowers, Robert Haas
 * Description: This program is a daily to-do list that records the name of 
 *              the activity, the description, the priority of the task, date 
 *              when the task needs to be finished, and status of the item
 *              (whether it has yet to be started, in-progress or finished).
 *              The programs functionality includes inserting new tasks,
 *              editing existing items, and removing tasks from the list.
 ******************************************************************************/

package CSE360Proj;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.print.PageLayout;
import javafx.print.PrinterJob;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ToDoListUnlimited extends Application {
	
	private int SIZE_WIDTH = 800, SIZE_HEIGHT = 600;

	private static ArrayList<List> taskList = new ArrayList<List>();

	private Stage mainWindow;
	private Label nameLabel;
	private Label descLabel;
	private Label priorityLabel;
	private Label dueLabel;
	private Label statusLabel;
	private Label sortLabel;
	private static TextField nameTextField;
	private static TextArea descTextArea;
	private static TextField priorityTextField;
	private static DatePicker dueDatePicker;
	private static ComboBox<String> sortBox;
	private static ComboBox<String> statusBox;

	static ListView<List> todoList;
	private static ErrorWindows error = new ErrorWindows();
	static String filename = "";

	@Override
	public void start(Stage primaryStage) {

		mainWindow = primaryStage;
		mainWindow.setTitle("To Do List Unlimited 2019");

		nameLabel = new Label(" Name: ");
		descLabel = new Label(" Description: ");
		priorityLabel = new Label(" Priority: ");
		dueLabel = new Label(" Due Date: ");
		statusLabel = new Label(" Status: ");
		sortLabel = new Label(" Sort by: ");

		nameTextField = new TextField();
		descTextArea = new TextArea();
		double heightTA = 60;
		double widthTA = 10;
		descTextArea.setPrefHeight(heightTA);
		descTextArea.setPrefWidth(widthTA);

		priorityTextField = new TextField();

		dueDatePicker = new DatePicker();

		sortBox = new ComboBox<String>();
		sortBox.getItems().addAll(
				"Name",
				"Priority",
				"Due Date",
				"Status"
		);

		EventHandler<ActionEvent> event = e -> {
            switch(sortBox.getValue().toString()) {
                case "Name": sortByName(); break;
                case "Priority": sortByPriority(); break;
                case "Due Date": sortByDueDate(); break;
                case "Status": sortByStatus(); break;
            }
		};
        sortBox.setOnAction(event);

		statusBox = new ComboBox<String>();
		statusBox.getItems().addAll(
				"Not Started",
				"In progress",
				"Finished"
		);

		Button insertButton = new Button("Insert");
		insertButton.setOnAction(e -> {
			if(nameTextField.getText().equals("") || descTextArea.getText().equals("") ||
			   priorityTextField.getText().equals("") || statusBox.getValue() == null){
				error.errorWindow("Missing Info");
			}

			else if(dueDatePicker.getValue() == null){
				error.errorWindow("Invalid Date");
			}

			else {
				insertTask();
			}
		});

		Button deleteButton = new Button("Delete");
		deleteButton.setOnAction(e -> deleteTask());
		Button updateButton = new Button("Update");
		updateButton.setOnAction(e -> changeInfo.updateButtonWindow());
		Button saveButton = new Button("Save");
		saveButton.setOnAction(e -> saveList());
		Button printButton = new Button("Print");
		printButton.setOnAction(e -> printList());
		Button startOverButton = new Button("Start Over");
		startOverButton.setOnAction(e -> changeInfo.chooseFromTwoWindow("StartOver/Cancel"));
		Button exitButton = new Button("Exit");
		exitButton.setOnAction(e -> {
			changeInfo.chooseFromTwoWindow("Save/Discard");
			Platform.exit();
		});

		todoList = new ListView<List>();
		todoList.setEditable(true);

		GridPane infoPanel = new GridPane();
		infoPanel.setMinSize(100, 100);
		infoPanel.setAlignment(Pos.CENTER);
		infoPanel.setHgap(20);
		infoPanel.setVgap(5);
		infoPanel.add(nameLabel, 0, 0);
		infoPanel.add(nameTextField, 1, 0);
		infoPanel.add(descLabel, 0, 1);
		infoPanel.add(descTextArea, 1, 1);
		infoPanel.add(priorityLabel, 0, 2);
		infoPanel.add(priorityTextField, 1, 2);
		infoPanel.add(dueLabel, 0, 3);
		infoPanel.add(dueDatePicker, 1, 3);
		infoPanel.add(statusLabel, 0, 4);
		infoPanel.add(statusBox, 1, 4);

		HBox buttonGroup = new HBox();
		buttonGroup.setAlignment(Pos.CENTER);
		buttonGroup.setSpacing(5);
		buttonGroup.getChildren().add(insertButton);
		buttonGroup.getChildren().add(updateButton);
		buttonGroup.getChildren().add(deleteButton);

		HBox sortGroup = new HBox();
		sortGroup.setAlignment(Pos.CENTER);
		sortGroup.setSpacing(10);
		sortGroup.getChildren().add(sortLabel);
		sortGroup.getChildren().add(sortBox);

		HBox saveGroup = new HBox();
		saveGroup.setAlignment(Pos.CENTER);
		saveGroup.setSpacing(5);
		saveGroup.getChildren().add(saveButton);
		saveGroup.getChildren().add(printButton);
		saveGroup.getChildren().add(startOverButton);


		VBox leftPanel = new VBox(20);
		leftPanel.setAlignment(Pos.CENTER);
		leftPanel.getChildren().add(infoPanel);
		leftPanel.getChildren().add(buttonGroup);
		leftPanel.getChildren().add(sortGroup);
		leftPanel.getChildren().add(saveGroup);
		leftPanel.getChildren().add(exitButton);

		BorderPane mainPane = new BorderPane();
		mainPane.setLeft(leftPanel);
		mainPane.setCenter(todoList);

		changeInfo.openFile();

		Scene scene = new Scene(mainPane, SIZE_WIDTH, SIZE_HEIGHT);
		mainWindow.setScene(scene);
		mainWindow.show();
	}
	
	private void insertTask() {
		String name = nameTextField.getText();
		String desc = descTextArea.getText();
		String date = dueDatePicker.getValue().toString();
		String status = statusBox.getValue();
		boolean duplicateName = false;
		boolean duplicateDescription = false;
		boolean duplicatePriority = false;

		int priority = -1;

		try {
			priority = Integer.parseInt(priorityTextField.getText());

			for (List task: taskList) {
				if (task.getName().equals(name)) {
					duplicateName = true;
				}

				if (task.getDesc().equals(desc)) {
					duplicateDescription = true;
				}

				if (task.getPriority() == priority) {
					duplicatePriority = true;
				}
			}

			if (duplicatePriority) {
				for (List task: taskList) {
					if (task.getPriority() >= priority) {
						task.setPriority(task.getPriority() + 1);
					}
				}
			}

			if (duplicateName) {
				error.errorWindow("Duplicate Name");
			}

			else if (duplicateDescription) {
				error.errorWindow("Duplicate Description");
			}

			else if (priority > taskList.size() + 1) {
				error.errorWindow("Invalid Priority");
			}

			else {
				List item = new List(name,desc,priority,date,status);
				taskList.add(item);

				nameTextField.clear();
				descTextArea.clear();
				priorityTextField.clear();
				dueDatePicker.setValue(null);
				statusBox.setValue(null);

				try {
                	switch(sortBox.getValue().toString()) {
                    	case "Name": sortByName(); break;
                    	case "Priority": sortByPriority(); break;
                    	case "Due Date": sortByDueDate(); break;
                    	case "Status": sortByStatus(); break;
                	}
				} catch (NullPointerException ignored) {}

				todoList.getItems().clear();
				todoList.getItems().addAll(taskList);
			}
		} catch (NumberFormatException e) {
			error.errorWindow("Non-numeric Priority");
		}
	}
	
	static boolean updateTask(int selected) {
		boolean validInput = true;
		try {
			if (selected >= 0 && selected < taskList.size()) {
				validInput = changeInfo.update(selected, taskList.get(selected), taskList);

                switch(sortBox.getValue().toString()) {
                    case "Name": sortByName(); break;
                    case "Priority": sortByPriority(); break;
                    case "Due Date": sortByDueDate(); break;
                    case "Status": sortByStatus(); break;
                }

				todoList.getItems().clear();
				todoList.getItems().addAll(taskList);
			}
			else
				error.errorWindow("Invalid Selection");

		} catch(IndexOutOfBoundsException | NullPointerException ignored) {}

		return validInput;
	}
	
	private static void deleteTask() {
		int selected = todoList.getSelectionModel().getSelectedIndex();
		int priority = taskList.get(selected).getPriority();
		taskList.remove(selected);

		for (List task: taskList) {
			if (task.getPriority() >= priority) {
				task.setPriority(task.getPriority() - 1);
			}
		}

		try {
            switch(sortBox.getValue().toString()) {
                case "Name": sortByName(); break;
                case "Priority": sortByPriority(); break;
                case "Due Date": sortByDueDate(); break;
                case "Status": sortByStatus(); break;
            }
		} catch (NullPointerException ignored) {}

		todoList.getItems().clear();
		todoList.getItems().addAll(taskList);
	}
	
	private static String listToText() {
		StringBuilder list = new StringBuilder();
		for (List task: taskList) {
			list.append(task.toString());
			list.append("\n\n");
		}

		return list.toString();
	}
	
	private static void sortByName() {
		taskList.sort(List.nameComparator);
		todoList.getItems().clear();
		todoList.getItems().addAll(taskList);
	}
	
	private static void sortByPriority() {
		taskList.sort(List.priorityComparator);
		todoList.getItems().clear();
		todoList.getItems().addAll(taskList);
	}
	
	private static void sortByDueDate() {
		taskList.sort(List.dueDateComparator);
		todoList.getItems().clear();
		todoList.getItems().addAll(taskList);
	}
	
	private static void sortByStatus() {
		taskList.sort(List.statusComparator);
		todoList.getItems().clear();
		todoList.getItems().addAll(taskList);
	}
			
	static void saveList() {
		if (filename.equals("")) {
			changeInfo.nameFile();
		}
		try {
			FileWriter writer = new FileWriter(filename, false);
			writer.write(listToText());
			writer.close();
			changeInfo.savedPrintedWindow("Save");
        } catch (IOException e) {
        	error.errorWindow("File Not Found");
        }
	}
	
	private static void printList() {
		try {
			TextFlow printArea = new TextFlow(new Text(listToText()));
			PrinterJob printerJob = PrinterJob.createPrinterJob();

			if(printerJob != null && printerJob.showPrintDialog(null)) {
				PageLayout pageLayout = printerJob.getJobSettings().getPageLayout();
				printArea.setMaxWidth(pageLayout.getPrintableWidth());

				if(printerJob.printPage(printArea)){
					printerJob.endJob();
				} else {
					error.errorWindow("Failed to Print");
				}

			} else {
				error.errorWindow("Print Canceled");
			}

		} catch (NullPointerException e) {
			error.errorWindow("Failed to Print");
		}
	}
	
	static void startOver() {
		taskList.clear();
		todoList.getItems().clear();
		todoList.getItems().addAll(taskList);
		nameTextField.clear();
		descTextArea.clear();
		priorityTextField.clear();
		dueDatePicker.setValue(null);
		statusBox.setValue(null);
		filename = "";
	}
	
	static boolean restoreList(String fileName) {
		boolean validInput = true;
        try {
            FileReader reader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line, name = "", desc = "", date = "", status = "";
    		int priority = 0;

            while ((line = bufferedReader.readLine()) != null) {
              	int colon = line.indexOf(':');

            	if (colon >= 0) {
	            	String label = line.substring(0, colon);
	            	String value = line.substring(colon + 2);

	            	switch (label) {
		            	case "Name": name = value; break;
		            	case " Description": desc = value; break;
		            	case " Priority": priority = Integer.parseInt(value); break;
		            	case " Due date": date = value; break;
		            	case " Status": status = value; break;
	            	}
            	}

            	else {
            		List item = new List(name,desc,priority,date,status);
            		taskList.add(item);
            	}
            }

    		todoList.getItems().clear();
    		todoList.getItems().addAll(taskList);

            reader.close();

        } catch (IOException e) {
        	error.errorWindow("File Not Found");
        	validInput = false;
        }

        return validInput;
	}


	public static void main(String[] args){
		launch(args);
	}
}
