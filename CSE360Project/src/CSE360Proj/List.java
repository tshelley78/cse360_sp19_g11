/*******************************************************************************
 * CSE360 Project: To-Do List Unlimited 2019
 * File: List.java
 * @authors: Thomas Shelley, Rebecca Martin, William Bowers, Robert Haas
 * Description: List objects are individual items in the To Do List.  Each 
 *              task stores their name, description, priority, due date, and 
 *              status in global variables, each with their own accessors and
 *              mutators.  This class also contains a toString method and 
 *              comparators for name, priority, due date, and status.    
 ******************************************************************************/

package CSE360Proj;

import java.util.Comparator;

public class List {

	private String name, desc, dueDate, status;
	private int priorityNum;
	
	List(String nameInput, String descInput, int priorityInput, String dueDateInput, String statusInput) {
		name = nameInput;
		desc = descInput;
		priorityNum = priorityInput;
		dueDate = dueDateInput;
		status = statusInput;
	}
	
	void setName(String name) {
		this.name = name;
	}
	
	void setDesc(String desc) {
		this.desc = desc;
	}
	
	void setPriority(int priority) {
		priorityNum = priority;
	}
	
	void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	
	void setStatus(String status) {
		this.status = status;
	}
	
	String getName() {
		return name;
	}
	
	String getDesc() {
		return desc;
	}
	
	int getPriority() {
		return priorityNum;
	}
	
	private String getDueDate() {
		return dueDate;
	}
	
	private String getStatus() {
		return status;
	}

	public String toString() {
		return ("Name: " + getName() + "\n Description: " + getDesc() + "\n Priority: " + 
                getPriority() +	"\n Due date: " + getDueDate() + "\n Status: " + getStatus());
	}
	
	static Comparator<List> nameComparator = (task1, task2) -> Integer.compare(0, task2.getName().compareToIgnoreCase(task1.getName()));
	
	static Comparator<List> priorityComparator = Comparator.comparingInt(List::getPriority);
	
	static Comparator<List> dueDateComparator = (task1, task2) -> Integer.compare(0, task2.getDueDate().compareToIgnoreCase(task1.getDueDate()));
	
	static Comparator<List> statusComparator = (task1, task2) -> Integer.compare(task2.getStatus().compareToIgnoreCase(task1.getStatus()), 0);	
}
